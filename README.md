# SeatbeltMonitor

SeatbeltMonitor is a very flexible module to monitor seat buckles and alert driver and passengers if someone removes his seatbelt. It doesn't use weight sensors, only buckles with sensors.

#### Main features:
- Easy installation - no weight sensors required, so no seat disassembly needed
- Visual and audible indication when seatbelt is unfastened
- Auto reset indicators in 60s for non-driver buckles
- Manual reset indicators for non-driver buckles
- Service procedure allowing to disable audible indication (only), without the need of a computer

## Supported cars

Car brand doesn't matter as long as:
1. Used buckles pass GND when NOT fastened and are not connected to other electronics
3. Parking brake supplies GND when applied
4. Output indicator is controlled by GND

I've developed the module for my 2001 Mazda 626 GF, which uses buckles from Mazda 6 GG FL and indicator from Mazda Protege.

## Installation video

[![](http://img.youtube.com/vi/eSX0CyAiRFI/0.jpg)](https://www.youtube.com/watch?v=eSX0CyAiRFI "")

https://www.youtube.com/watch?v=eSX0CyAiRFI

## Hardware

The project makes use of **Arduino Pro Mini**, but you could use any other **ATmega8/168/328**-based board.

### Power consumption

The module is meant to be running only when the engine is running, so no consumption optimizations are done to it.

### Box

I've included a simple box (as seen in the video) that you can 3D print yourself. Check [SeatbeltMonitorBox.stl](box/SeatbeltMonitorBox.stl). Do not use PLA, as it melts when the car is left under the sun (as seen in the video).

**Note**: Tolerances are non-existent. This is done for 2 reasons - that allows a simple box without a cap, and fits the limited space I have (as seen in the video). Use a mini grinding tool to remove as much material as need from the Arduino.

## Software

Source code is written with [Arduino IDE](https://www.arduino.cc/) 2.0.

Code is very simple, and it fits just fine in a single file - [SeatbeltMonitor.ino](src/SeatbeltMonitor/SeatbeltMonitor.ino)

## Schematics

![](schematics/SeatbeltMonitor.png)

| Item             | Type                              | Qty  | Purpose                                           |
| :---             | :---                              | :--- | :---                                              |
| Arduino Pro Mini | Microcontroller                   | 1    | The brain                                         |
| AP2310GN-HF-3TR  | NMOS Transistor                   | 1    | Controls incandescent light bulb by ground signal |
| P6KE33A          | Transil Diode                     | 1    | Protects the circuit                              |
| 1N5819           | Schottky Diode                    | 1    | Over-voltage protection                           |
| 5.1kΩ            | Resistor                          | 6    | Pull-up resistor                                  |
| 10kΩ             | Resistor                          | 1    | Transistors protection                            |
| 100nF            | Capacity                          | 2    | Stabilize pins used in interrupts                 |
| 4kHz             | Piezoelectric Buzzer \w generator | 1    | Audible indicator                                 |

Schematics have been done with [Circuit Diagram](https://www.circuit-diagram.org/). Check [schematics](schematics) folder.

Odd letters seen on near some lines are the cable colors as seen in the video. I've followed the naming convention found in all Mazda wiring diagram books, and probably other manufacturers as well.

## Installation

Module is very flexible. Each input can be skipped - not only wiring, but also soldering. For an instance, if only 2 seats, you can skip soldering pull-up resistors for rear seats and no wiring needed. Parking brake is also optional.

**Note**: Cables for non-driver buckles are interchangeable, meaning you can connect the rear left buckle to the passenger signal and this won't cause any issues.

### Flashing the Arduino

In case you never did that, here are the steps:

1. Download [Arduino IDE](https://www.arduino.cc/).
2. Download the [src](src) folder.
3. Connect the programmer to the Arduino. If your programmer has each cable separate, this is the typical connection:
   - Arduino TX -> White cable
   - Arduino RX -> Green cable
   - Arduino Vcc -> Red cable
   - Arduino GND -> Black cable
4. Plug-in the programmer to the USB port of the computer. At that point, you should see the Power LED get constantly lid. Also, it is normal to see the other built-in LED to start blinking, as many Arduino boards come with Blink example pre-installed.
5. Install drivers for the programmer if needed. I cannot give you steps here, as it depends on the programmer and OS you are using.
6. Open [SeatbeltMonitor.ino](src/SeatbeltMonitor/SeatbeltMonitor.ino) with Arduino IDE.
7. From the Tools -> Boards menu, select **Arduino Pro or Pro Mini** (or your model, if you are not using this one).
8. From the Tools -> Port menu, select the port of your programmer (lookup Google how to find it, as it is OS dependent).
9. From the Tools -> Processor menu, make sure the proper processor is selected.
10. From the Tools -> Programmer menu, select your programmer.
11. Flash by pressing the Upload button on the toolbar or Sketch -> Upload. **Note**: If your programmer has only 4 wires, then you'll need to press the Reset button on the Arduino board as soon as you press Upload.

### Flashing without the bootloader (for faster startup)

On my Mazda, when you turn the ignition to **ON**, all lights on the dashboard turn on and in a bit some of them turn off. I wanted to mimic that behavior, but the problem is the bootloader of the Arduino Pro Mini is way too slow, adds 1.5-2 seconds to the boot time! By the time the visual indicator turns on, other indicators are about to turn off... So I needed to flash the sketch without a bootloader, which made startup almost instant (there is still a little delay, but you need a sharp eye to catch it). The procedure:

1. Get a second Arduino. I repurposed another Pro Mini with a burnt out pin.
2. Flash that second Arduino with the **ArduinoISP** example (from the **File** menu). This Arduino will now become the programmer.
3. Solder around 10nF capacitor between **GND** and **RST** pins of the **programmer** Arduino. I didn't have 10nF, 4.7nF did the trick.
4. Connect Programmer pins -> Arduino Pro Mini pins as follows:
   1. GND -> GND
   2. Vcc -> Vcc
   3. Pin 10 -> RST
   4. Pin 11 -> Pin 11
   5. Pin 12 -> Pin 12
   6. Pin 13 -> Pin 13
5. In **Arduino IDE**, select Programmer to be **Arduino as ISP**
6. Upload by using **Upload Using Programmer** option in **Sketch** menu.

## Disable audible indication
Beep can be disabled and re-enabled by the same procedure:
1. Make sure ignition is **OFF**.
2. Make sure driver seat belt is **NOT** fastened.
4. Pull the parking brake.
5. Turn the ignition to **ON** (no need to start the engine).
6. Within 30 seconds, fasten and unfasten driver buckle.

If all done correctly, visual and audible indicators will be turned on for 2 seconds.

## Show software version
To show currently flashed version:
1. Make sure ignition is **OFF**.
2. Make sure driver seat belt is fastened.
3. Make sure parking brake is **NOT ON**.
4. Turn the ignition to **ON** (no need to start the engine).
5. Within 10 seconds, pull and release the parking brake 5 times.

If all done correctly, visual and audible indicators will be turned on in the following pattern:
1. **LONG** duration indicates major version (1 **LONG** is v1.X, 2 **LONG** is v2.X, etc.)
2. **SHORT** duration indicates minor version (1 **SHORT** means vX.1, 2 **SHORT** means vX.2, etc.). This will be skipped if 0.

Example: v1.0 is only 1 **LONG** indication. v2.1 will be **LONG**-**LONG**-**SHORT**.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.