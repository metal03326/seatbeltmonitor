#include <EEPROM.h>

#define VER 1.0

#define i_brake 2
#define i_driver 3
#define i_right 10
#define i_center 11
#define i_left 12
#define i_pax A0
#define o_indicator 6
#define o_beep 9

#define initial_test_duration 2300

// This code is copied from http://masteringarduino.blogspot.com/2013/10/fastest-and-smallest-digitalread-and.html
#define portOfPin(P) \
  (((P) >= 0 && (P) < 8) ? &PORTD : (((P) > 7 && (P) < 14) ? &PORTB : &PORTC))
#define ddrOfPin(P) \
  (((P) >= 0 && (P) < 8) ? &DDRD : (((P) > 7 && (P) < 14) ? &DDRB : &DDRC))
#define pinOfPin(P) \
  (((P) >= 0 && (P) < 8) ? &PIND : (((P) > 7 && (P) < 14) ? &PINB : &PINC))
#define pinIndex(P) ((uint8_t)(P > 13 ? P - 14 : P & 7))
#define pinMask(P) ((uint8_t)(1 << pinIndex(P)))
#define pinAsInput(P) *(ddrOfPin(P)) &= ~pinMask(P)
#define pinAsInputPullUp(P) \
  *(ddrOfPin(P)) &= ~pinMask(P); \
  digitalHigh(P)
#define pinAsOutput(P) *(ddrOfPin(P)) |= pinMask(P)
#define digitalLow(P) *(portOfPin(P)) &= ~pinMask(P)
#define digitalHigh(P) *(portOfPin(P)) |= pinMask(P)
#define isHigh(P) ((*(pinOfPin(P)) & pinMask(P)) > 0)
#define isLow(P) ((*(pinOfPin(P)) & pinMask(P)) == 0)
#define digitalState(P) ((uint8_t)isHigh(P))

volatile bool right;
volatile bool center;
volatile bool left;
volatile bool pax;
volatile byte brakeCounter;
volatile byte driverBuckleCounter;
unsigned long beepTimer;
bool beepEnabled;
bool inInitialBrakeActionInterval;
bool inInitialDriverActionInterval;

void setup() {
  pinAsOutput(o_indicator);
  // Light up visual indicator as soon as possible
  digitalHigh(o_indicator);

  pinAsInputPullUp(i_brake);
  pinAsInputPullUp(i_driver);
  pinAsInputPullUp(i_right);
  pinAsInputPullUp(i_center);
  pinAsInputPullUp(i_left);
  pinAsInputPullUp(i_pax);
  pinAsOutput(o_beep);

  attachInterrupt(digitalPinToInterrupt(i_brake), onBrakeReleased, RISING);
  attachInterrupt(digitalPinToInterrupt(i_driver), onDriverBuckled, RISING);
  digitalHigh(o_beep);

  // Test output indicator. Adjust ms to fit your car's indicator test turn-off timer.
  // PS: Remove bootloader for this to properly work
  delay(initial_test_duration);

  digitalLow(o_indicator);

  takeBucklesSnapshot();

  brakeCounter = 0;
  driverBuckleCounter = 0;

  beepEnabled = EEPROM.read(0) == 255;

  inInitialBrakeActionInterval = true;
  inInitialDriverActionInterval = true;
}

bool isBuckleFastened(byte pin) {
  // All buckles share the same logic - if HIGH then no GND is coming, so buckle is fastened
  return isHigh(pin);
}

bool isBrakeOn() {
  // If i_brake is HIGH then no GND is coming, so brake is NOT on
  return isLow(i_brake);
}

void feedbackBlink(byte times, int wait) {
  for (byte i = 0; i < times; i++) {
    digitalLow(o_beep);
    digitalHigh(o_indicator);
    delay(wait);
    digitalHigh(o_beep);
    digitalLow(o_indicator);
    delay(500);
  }
}

void showVersion() {
  const byte majorVersion = (byte)VER;
  const float minorVersion = VER - (float)majorVersion;

  feedbackBlink(majorVersion, 1000);

  if (minorVersion > 0) {
    feedbackBlink((int)(minorVersion / 0.1), 500);
  }

  brakeCounter = 0;
}

void toggleAudibleSetting() {
  feedbackBlink(1, 2000);

  beepEnabled = !beepEnabled;

  EEPROM.write(0, beepEnabled ? 255 : 0);

  driverBuckleCounter = 0;
}

void onBrakeReleased() {
  takeBucklesSnapshot();

  if (inInitialBrakeActionInterval && brakeCounter < 5 && isBuckleFastened(i_driver)) {
    brakeCounter++;
  }
}

void onDriverBuckled() {
  takeBucklesSnapshot();

  if (inInitialDriverActionInterval && driverBuckleCounter < 20 && isBrakeOn()) {
    driverBuckleCounter++;
  }
}

void takeBucklesSnapshot() {
  right = isBuckleFastened(i_right);
  center = isBuckleFastened(i_center);
  left = isBuckleFastened(i_left);
  pax = isBuckleFastened(i_pax);

  // Always start beeping from the beginning if driver got buckled or brake got released
  if (isBuckleFastened(i_driver) || isBrakeOn()) {
    beepTimer = 0;
  }
}

void alert(bool start) {
  // Show visually
  if (start) {
    digitalHigh(o_indicator);
  } else {
    digitalLow(o_indicator);
  }

  // Show audiblyly
  beep(start);

  // Start or stop 60s timer for buckle change, if brake is not on
  if (start && !isBrakeOn()) {
    unsigned long time = millis();

    if (beepTimer == 0) {
      beepTimer = time;
    } else if (60000 < time - beepTimer) {
      // Re-detect buckles. This will stop alarm next round, if it was not the driver
      // buckle triggering it. Otherwise it'll just start a new 60 second timer.
      takeBucklesSnapshot();
    }
  } else {
    beepTimer = 0;
  }
}

void beep(bool start) {
  // Do not beep if brake is on
  bool inPhase = beepEnabled && start && !isBrakeOn();

  // If we need to beep, see if we are actually in beep phase
  if (inPhase) {
    unsigned long time = millis();
    long diff = time - beepTimer;
    int mod = diff % 1000;

    if (diff < 15000) {
      // 500ms ON, 500ms OFF (1 beep per second)
      inPhase = mod < 500;
    } else if (diff < 30000) {
      // 300ms ON, 200ms OFF (2 beeps per second)
      inPhase = mod < 300 || mod > 500 && mod < 800;
    } else if (diff < 45000) {
      // 220ms ON, 113ms OFF (3 beeps per second)
      inPhase = mod < 220 || mod > 333 && mod < 553 || mod > 666 && mod < 886;
    } else {
      // 170ms ON, 80ms OFF (4 beeps per second)
      inPhase = mod < 170 || mod > 250 && mod < 420 || mod > 500 && mod < 670 || mod > 750 && mod < 920;
    }
  }

  if (inPhase) {
    digitalLow(o_beep);
  } else {
    digitalHigh(o_beep);
  }
}

void loop() {
  // First 10 seconds will monitor brake for version display functionality
  if (inInitialBrakeActionInterval && millis() > (10000 + initial_test_duration)) {
    inInitialBrakeActionInterval = false;
  }

  // First 30 seconds will monitor driver buckle for disable beep functionality
  if (inInitialDriverActionInterval && millis() > (30000 + initial_test_duration)) {
    inInitialDriverActionInterval = false;
  }

  if (brakeCounter == 5) {
    showVersion();
  } else if (driverBuckleCounter == 20) {
    toggleAudibleSetting();
  } else {
    bool pax_now = isBuckleFastened(i_pax);
    bool right_now = isBuckleFastened(i_right);
    bool center_now = isBuckleFastened(i_center);
    bool left_now = isBuckleFastened(i_left);

    // If seat belt got buckled, save state
    if (
      !pax && pax_now
      || !right && right_now
      || !center && center_now
      || !left && left_now) {
      takeBucklesSnapshot();
    }

    alert(
      !isBuckleFastened(i_driver)
      || pax != pax_now
      || right != right_now
      || center != center_now
      || left != left_now);
  }
}